package de.schegge.leitweg;

import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public enum BundesAdressierung implements GrobAdressierung {
    ZRE(RechnungseingangsPlattform.ZRE),
    OZG_RE(RechnungseingangsPlattform.OZG_RE),
    ANDERE(RechnungseingangsPlattform.ANDERE);

    public static final Map<String, BundesAdressierung> PLATTFORM_MAP = Map.of("1", ZRE, "2", OZG_RE, "3", ANDERE);

    private final RechnungseingangsPlattform plattform;

    BundesAdressierung(RechnungseingangsPlattform plattform) {
        this.plattform = requireNonNull(plattform);
    }

    @Override
    public String toString() {
        return "99" + plattform.getValue();
    }

    @Override
    public Optional<BundesAdressierung> asBundesAdressierung() {
        return Optional.of(this);
    }

    public RechnungseingangsPlattform getPlattform() {
        return plattform;
    }

    public static Optional<BundesAdressierung> byId(String id) {
        return Optional.ofNullable(PLATTFORM_MAP.get(id));
    }

}
