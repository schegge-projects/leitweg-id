package de.schegge.leitweg;

public enum RechnungseingangsPlattform {
    ZRE,
    OZG_RE,
    ANDERE;

    String getValue() {
        return String.valueOf(ordinal() + 1);
    }
}
