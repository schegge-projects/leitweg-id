package de.schegge.leitweg;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class LandesAdressierung implements GrobAdressierung {

    private final GermanFederalState bundesland;
    private final String regierungsbezirk;
    private final String landkreis;
    private final String gemeinde;

    LandesAdressierung(GermanFederalState bundesland, String regierungsbezirk, String landkreis, String gemeinde) {
        this.bundesland = requireNonNull(bundesland);
        this.regierungsbezirk = requireNonNull(regierungsbezirk);
        this.landkreis = requireNonNull(landkreis);
        this.gemeinde = requireNonNull(gemeinde);
    }

    @Override
    public String toString() {
        return bundesland.getId() + regierungsbezirk + landkreis + gemeinde;
    }

    @Override
    public Optional<LandesAdressierung> asLandesAdressierung() {
        return Optional.of(this);
    }

    public GermanFederalState getBundesland() {
        return bundesland;
    }

    public Optional<String> getRegierungsbezirk() {
        return regierungsbezirk.isEmpty() ? Optional.empty() : Optional.of(regierungsbezirk);
    }

    public Optional<String> getLandkreis() {
        return landkreis.isEmpty() ? Optional.empty() : Optional.of(landkreis);
    }

    public Optional<String> getGemeinde() {
        return gemeinde.isEmpty() ? Optional.empty() : Optional.of(gemeinde);
    }
}
