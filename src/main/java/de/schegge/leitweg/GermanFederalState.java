package de.schegge.leitweg;

import java.util.Optional;
import java.util.stream.Stream;

public enum GermanFederalState {
    BW("Baden-Württemberg", "08"), BY("Bayern", "09"), BE("Berlin", "11"),
    BB("Brandenburg", "12"), HB("Bremen", "04"), HH("Hamburg", "02"),
    HE("Hessen", "06"), MV("Mecklenburg-Vorpommern", "13"), NI("Niedersachsen", "03"),
    NW("Nordrhein-Westfalen", "05"), RP("Rheinland-Pfalz", "07"), SL("Saarland", "10"),
    SN("Sachsen", "14"), ST("Sachsen-Anhalt", "15"), SH("Schleswig-Holstein", "01"),
    TH("Thüringen", "16");

    private final String name;
    private final String id;

    GermanFederalState(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public static Optional<GermanFederalState> byId(String id) {
        return Stream.of(values()).filter(x -> id.equals(x.id)).findFirst();
    }
}
