package de.schegge.leitweg.module;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.databind.util.ClassUtil;
import de.schegge.leitweg.LeitwegId;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class LeitwegIdModule extends SimpleModule {
    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(new SimpleSerializers(List.of(new StdSerializer<>(LeitwegId.class) {
            @Override
            public void serialize(LeitwegId leitwegId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                jsonGenerator.writeString(leitwegId.toString());
            }
        })));
        context.addDeserializers(new SimpleDeserializers(Map.of(LeitwegId.class, new StdDeserializer<LeitwegId>(LeitwegId.class) {
            @Override
            public LeitwegId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
                try {
                return LeitwegId.parse(jsonParser.getValueAsString());
                } catch (IllegalArgumentException e) {
                    return (LeitwegId) deserializationContext.handleWeirdStringValue(_valueClass, jsonParser.getValueAsString(),
                            "not a valid representation (error: %s)", ClassUtil.exceptionMessage(e));
                }
            }
        })));
    }
}
