package de.schegge.leitweg;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.Objects.requireNonNullElseGet;

import java.math.BigInteger;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class LeitwegId {

  private static final BigInteger VALUE_01 = BigInteger.valueOf(1);
  private static final BigInteger VALUE_97 = BigInteger.valueOf(97);
  private static final BigInteger VALUE_98 = BigInteger.valueOf(98);

  private static final Pattern pattern = Pattern
      .compile("^(\\d{2})((\\d)((\\d{2})(\\d{3}|\\d{4}|\\d{7})?)?)?(-([A-Za-z0-9]{1,30}))?-(\\d{2})$");

  private final GrobAdressierung grobAdressierung;
  private final String feinAdressierung;
  private String checksum;
  private String value;

  private LeitwegId(GrobAdressierung grobAdressierung, String feinAdressierung) {
    this.grobAdressierung = requireNonNull(grobAdressierung);
    this.feinAdressierung = requireNonNull(feinAdressierung);
  }

  public static LeitwegId from(GermanFederalState bundesland, String regierungsbezirk, String landkreis,
      String gemeinde, String feinAdressierung) {
    return new LeitwegId(new LandesAdressierung(bundesland, regierungsbezirk, landkreis, gemeinde), feinAdressierung);
  }

  public static LeitwegId from(GrobAdressierung grobAdressierung, String feinAdresssierung) {
    return new LeitwegId(grobAdressierung, feinAdresssierung);
  }

  public static LeitwegId parse(String value) {
    Matcher matcher = pattern.matcher(value);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("cannot parse id: " + value);
    }

    String countryOrState = matcher.group(1);
    GrobAdressierung grobAdressierung;
    if ("99".equals(countryOrState)) {
      if (matcher.group(5) != null || matcher.group(6) != null) {
        throw new IllegalArgumentException("cannot parse id: " + value);
      }
      grobAdressierung = BundesAdressierung.byId(matcher.group(3)).orElseThrow();
    } else {
      GermanFederalState bundesland = GermanFederalState.byId(countryOrState).orElseThrow();
      String regierungsbezirk = requireNonNullElse(matcher.group(3), "");
      String landkreis = requireNonNullElse(matcher.group(5), "");
      String gemeinde = requireNonNullElse(matcher.group(6), "");
      grobAdressierung = new LandesAdressierung(bundesland, regierungsbezirk, landkreis, gemeinde);
    }
    String feinAdressierung = requireNonNullElse(matcher.group(8), "");
    String checksum = matcher.group(9);
    return complete(proof(value, grobAdressierung, feinAdressierung, checksum), value, checksum);
  }

  private static LeitwegId complete(LeitwegId leitwegId, String value, String checksum) {
    leitwegId.checksum = checksum;
    leitwegId.value = value;
    return leitwegId;
  }

  private static LeitwegId proof(String value, GrobAdressierung grobAdressierung, String feinAdressierung,
      String checksum) {
    BigInteger number = generateNumber(grobAdressierung, feinAdressierung, checksum);
    if (!number.mod(VALUE_97).equals(VALUE_01)) {
      throw new IllegalArgumentException("invalid checksum: " + value);
    }
    return from(grobAdressierung, feinAdressierung);
  }

  private String calculateChecksum() {
    BigInteger number = generateNumber(grobAdressierung, feinAdressierung, "00");
    String result = VALUE_98.subtract(number.mod(VALUE_97)).toString();
    return result.length() == 1 ? "0" + result : result;
  }

  private static BigInteger generateNumber(GrobAdressierung grobAdressierung, String feinAdressierung,
      String checksum) {
    StringBuilder builder = new StringBuilder().append(grobAdressierung);
    for (char c : feinAdressierung.toCharArray()) {
      if (c >= '0' && c <= '9') {
        builder.append(c);
      } else if (c >= 'A' && c <= 'Z') {
        builder.append(c - 'A' + 10);
      } else {
        builder.append(c - 'a' + 10);
      }
    }
    return new BigInteger(builder.append(checksum).toString());
  }

  public GrobAdressierung getGrobAdressierung() {
    return grobAdressierung;
  }

  public Optional<String> getFeinAdressierung() {
    return feinAdressierung.isEmpty() ? Optional.empty() : Optional.of(feinAdressierung);
  }

  public String getChecksum() {
    checksum = requireNonNullElseGet(checksum, this::calculateChecksum);
    return checksum;
  }

  private String asString() {
    StringJoiner joiner = new StringJoiner("");
    joiner.add(grobAdressierung.toString());
    if (!feinAdressierung.isEmpty()) {
      joiner.add("-").add(feinAdressierung);
    }
    return joiner.add("-").add(getChecksum()).toString();
  }

  @Override
  public String toString() {
    value = requireNonNullElseGet(value, this::asString);
    return value;
  }
}
