package de.schegge.leitweg;

import java.util.Optional;

public interface GrobAdressierung {

    default Optional<LandesAdressierung> asLandesAdressierung() {
        return Optional.empty();
    }

    default Optional<BundesAdressierung> asBundesAdressierung() {
        return Optional.empty();
    }
}
