package de.schegge.leitweg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class LeitwegIdTest {

  @Test
  void parseBundesland() {
    LeitwegId leitwegId = LeitwegId.parse("01-95");
    GrobAdressierung grobAdressierung = leitwegId.getGrobAdressierung();
    LandesAdressierung landesAdressierung = grobAdressierung.asLandesAdressierung().orElseThrow();
    assertEquals(GermanFederalState.SH, landesAdressierung.getBundesland());
    assertTrue(landesAdressierung.getRegierungsbezirk().isEmpty());
    assertTrue(landesAdressierung.getLandkreis().isEmpty());
    assertTrue(landesAdressierung.getGemeinde().isEmpty());
    assertTrue(leitwegId.getFeinAdressierung().isEmpty());
    assertEquals("95", leitwegId.getChecksum());
  }

  @Test
  void parseBund() {
    LeitwegId leitwegId = LeitwegId.parse("992-80150-88");
    GrobAdressierung grobAdressierung = leitwegId.getGrobAdressierung();
    assertEquals("992", grobAdressierung.toString());
    BundesAdressierung bundesAdressierung = grobAdressierung.asBundesAdressierung().orElseThrow();
    assertEquals(RechnungseingangsPlattform.OZG_RE, bundesAdressierung.getPlattform());
    assertEquals("80150", leitwegId.getFeinAdressierung().orElseThrow());
    assertEquals("88", leitwegId.getChecksum());
  }

  @Test
  void fromBund() {
    LeitwegId leitwegId = LeitwegId.from(BundesAdressierung.OZG_RE, "80150");
    GrobAdressierung grobAdressierung = leitwegId.getGrobAdressierung();
    assertEquals("992", grobAdressierung.toString());
    BundesAdressierung bundesAdressierung = grobAdressierung.asBundesAdressierung().orElseThrow();
    assertEquals(RechnungseingangsPlattform.OZG_RE, bundesAdressierung.getPlattform());
    assertEquals("80150", leitwegId.getFeinAdressierung().orElseThrow());
    assertEquals("88", leitwegId.getChecksum());
    assertEquals("992-80150-88", leitwegId.toString());
  }

  @Test
  void parseDemo() {
    LeitwegId leitwegId = LeitwegId.parse("04011000-1234512345-06");
    GrobAdressierung grobAdressierung = leitwegId.getGrobAdressierung();
    assertEquals("04011000", grobAdressierung.toString());
    LandesAdressierung landesAdressierung = grobAdressierung.asLandesAdressierung().orElseThrow();
    assertEquals(GermanFederalState.HB, landesAdressierung.getBundesland());
    assertEquals("0", landesAdressierung.getRegierungsbezirk().orElseThrow());
    assertEquals("11", landesAdressierung.getLandkreis().orElseThrow());
    assertEquals("000", landesAdressierung.getGemeinde().orElseThrow());
    assertEquals("1234512345", leitwegId.getFeinAdressierung().orElseThrow());
    assertEquals("06", leitwegId.getChecksum());
  }

  @Test
  void fromDemo() {
    LeitwegId leitwegId = LeitwegId.from(GermanFederalState.HB, "0", "11", "000", "1234512345");
    GrobAdressierung grobAdressierung = leitwegId.getGrobAdressierung();
    assertEquals("04011000", grobAdressierung.toString());
    LandesAdressierung landesAdressierung = grobAdressierung.asLandesAdressierung().orElseThrow();
    assertEquals(GermanFederalState.HB, landesAdressierung.getBundesland());
    assertEquals("0", landesAdressierung.getRegierungsbezirk().orElseThrow());
    assertEquals("11", landesAdressierung.getLandkreis().orElseThrow());
    assertEquals("000", landesAdressierung.getGemeinde().orElseThrow());
    assertEquals("1234512345", leitwegId.getFeinAdressierung().orElseThrow());
    assertEquals("06", leitwegId.getChecksum());
    assertEquals("04011000-1234512345-06", leitwegId.toString());
  }

  @Test
  void parseUniBielefeld() {
    LeitwegId leitwegId = LeitwegId.parse("05711-06001-79");
    GrobAdressierung grobAdressierung = leitwegId.getGrobAdressierung();
    assertEquals("05711", grobAdressierung.toString());
    LandesAdressierung landesAdressierung = grobAdressierung.asLandesAdressierung().orElseThrow();
    assertEquals(GermanFederalState.NW, landesAdressierung.getBundesland());
    assertEquals("7", landesAdressierung.getRegierungsbezirk().orElseThrow());
    assertEquals("11", landesAdressierung.getLandkreis().orElseThrow());
    assertTrue(landesAdressierung.getGemeinde().isEmpty());
    assertEquals("06001", leitwegId.getFeinAdressierung().orElseThrow());
    assertEquals("79", leitwegId.getChecksum());
  }

  @Test
  void convertToString() {
    assertEquals("05711-06001-79", LeitwegId.parse("05711-06001-79").toString());
    assertEquals("04011000-1234512345-06", LeitwegId.parse("04011000-1234512345-06").toString());
  }
}