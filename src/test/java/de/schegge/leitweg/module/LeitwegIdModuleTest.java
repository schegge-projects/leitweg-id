package de.schegge.leitweg.module;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import de.schegge.leitweg.GermanFederalState;
import de.schegge.leitweg.LandesAdressierung;
import de.schegge.leitweg.LeitwegId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LeitwegIdModuleTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper().registerModule(new LeitwegIdModule());
    }

    @Test
    void deserialize() throws JsonProcessingException {
        LeitwegId leitwegId = objectMapper.readValue("\"053340002002-33004-23\"", LeitwegId.class);
        assertAll(
                () -> assertEquals("33004", leitwegId.getFeinAdressierung().orElseThrow()),
                () -> {
                    Optional<LandesAdressierung> landesAdressierung = leitwegId.getGrobAdressierung().asLandesAdressierung();
                    assertAll(
                            () -> assertEquals("0002002", landesAdressierung.flatMap(LandesAdressierung::getGemeinde).orElseThrow()),
                            () -> assertEquals("34", landesAdressierung.flatMap(LandesAdressierung::getLandkreis).orElseThrow()),
                            () -> assertEquals("3", landesAdressierung.flatMap(LandesAdressierung::getRegierungsbezirk).orElseThrow()),
                            () -> assertEquals(GermanFederalState.NW, landesAdressierung.map(LandesAdressierung::getBundesland).orElseThrow())
                    );
                },
                () -> assertEquals("23", leitwegId.getChecksum())
        );
    }

    @Test
    void deserializeInvalid() {
        InvalidFormatException invalidFormatException = assertThrows(InvalidFormatException.class,
                () -> objectMapper.readValue("\"053340002002-33004\"", LeitwegId.class));
        assertEquals(
                "Cannot deserialize value of type `de.schegge.leitweg.LeitwegId` from String \"053340002002-33004\": not a valid representation (error: cannot parse id: 053340002002-33004)\n" +
                        " at [Source: REDACTED (`StreamReadFeature.INCLUDE_SOURCE_IN_LOCATION` disabled); line: 1, column: 1]",
                invalidFormatException.getMessage());
    }

    @Test
    void serialize() throws JsonProcessingException {
        LeitwegId leitwegId = LeitwegId.parse("053340002002-33004-23");
        assertEquals("\"053340002002-33004-23\"", objectMapper.writeValueAsString(leitwegId));
    }
}